var gulp = require('gulp');
var cleanCSS = require('gulp-clean-css');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');

var del = require('del');
var recursiveConcat = require('gulp-recursive-concat');
var stylus= require('gulp-stylus');


var paths = {
  src: 'resources/**/*',
  srcCSS: 'resources/css/',
  srcJS: 'resources/js/',
  
  dist: '../static/',
  distCSS: '../static/css/',
  distJS: '../static/js/'
};

/***PRODDDDDDDDDDDDDDDD-*************/
gulp.task('css', function () {
  return gulp.src([
			paths.srcCSS + '**/*.styl',
			'!' + paths.srcCSS + '**/**/_**/*.styl',
			'!' + paths.srcCSS + '_**/*.styl',
			'!' + paths.srcCSS + '**/_*.styl'
		])
        .pipe(stylus({
            compress: true
        }))
        .pipe(cleanCSS())
		.pipe(gulp.dest(paths.distCSS));
});

gulp.task('js', function () {
  return gulp.src(paths.srcJS + '**/*.js')
		.pipe(recursiveConcat({extname: ".js",outside: true}))
        .pipe(uglify())
		.pipe(gulp.dest(paths.distJS));
});
/***end prod---------****************/

gulp.task('build', ['css', 'js']);
gulp.task('watch', function () {
  gulp.watch(paths.src, ['build']);
});
gulp.task('clean', function () {
    del([paths.dist]);
});

gulp.task('default', ['build','watch']);